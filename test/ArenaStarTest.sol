// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import "forge-std/Test.sol";
import "../src/ArenaStar.sol";

contract ArenaStarTest is Test {
    ArenaStar arena;

    error InvalidSigner();
    error RepeatMint();

    function setUp() public {
        arena = new ArenaStar();
    }

    function testURI() public {
        assertEq(arena.tokenURI(20), "https://gitlab.com/0xsuper9/changshahub/-/raw/main/nft/arena/1.json");

        arena.setBaseURI("http://github.com/");

        assertEq(arena.tokenURI(10), "http://github.com/1.json");
        assertEq(arena.tokenURI(100), "http://github.com/2.json");
        assertEq(arena.tokenURI(199), "http://github.com/2.json");
        assertEq(arena.tokenURI(200), "http://github.com/3.json");
        assertEq(arena.tokenURI(1001), "http://github.com/11.json");
        assertEq(arena.tokenURI(1201), "http://github.com/13.json");
        assertEq(arena.tokenURI(1691), "http://github.com/17.json");
    }

    function testAirdrop() public {
        address[] memory list = new address[](3);
        list[0] = makeAddr("alice");
        list[1] = makeAddr("bob");
        list[2] = makeAddr("alice");
        arena.airdrop(list);

        assertEq(arena.ownerOf(1), list[0]);
        assertEq(arena.ownerOf(2), list[1]);
        assertEq(arena.ownerOf(3), list[2]);

        address[] memory list2 = new address[](2);
        list2[0] = makeAddr("eva");
        list2[1] = makeAddr("bob");
        arena.airdrop(list2);

        assertEq(arena.ownerOf(4), list2[0]);
        assertEq(arena.ownerOf(5), list2[1]);
    }

    function testNext() public {
        arena.setId(100);
        assertEq(arena.currentId(), 100);
    }

    function testMint() public {
        (address admin, uint256 pk) = makeAddrAndKey("admin");

        address alice = makeAddr("alice");
        bytes32 message = keccak256(abi.encodePacked("AS#01", uint256(100)));
        bytes32 digest = keccak256(abi.encodePacked(alice, message));
        (uint8 v, bytes32 r, bytes32 s) = vm.sign(pk, digest);

        vm.expectRevert(InvalidSigner.selector);
        arena.mint(message, v, r, s);

        //change admin
        arena.transferOwnership(admin);

        uint256 amount = arena.balanceOf(alice);
        changePrank(alice);
        arena.mint(message, v, r, s);
        assertEq(amount + 1, arena.balanceOf(alice)); // +1

        changePrank(alice);
        vm.expectRevert(RepeatMint.selector);
        arena.mint(message, v, r, s);
    }
}
