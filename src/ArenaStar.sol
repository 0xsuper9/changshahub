// SPDX-License-Identifier: MIT

pragma solidity 0.8.19;

import "solmate/tokens/ERC721.sol";
import "solmate/utils/LibString.sol";
import "solmate/auth/Owned.sol";

///////////////////////////////
//                           //
//                           //
//    ArenaStar by Suphub    //
//                           //
//                           //
///////////////////////////////

contract ArenaStar is Owned, ERC721 {
    string private _baseUri;
    uint256 public currentId;
    mapping(bytes32 => bool) minted;

    error InvalidSigner();
    error RepeatMint();

    constructor() ERC721("ArenaStar", "ARENA") Owned(msg.sender) {
        _baseUri = "https://gitlab.com/0xsuper9/changshahub/-/raw/main/nft/arena/";
    }

    function tokenURI(uint256 id) public view override returns (string memory) {
        return string.concat(_baseUri, LibString.toString(id / 100 + 1), ".json");
    }

    function setBaseURI(string calldata baseUri) external onlyOwner {
        _baseUri = baseUri;
    }

    function setId(uint256 nextId) external onlyOwner {
        require(nextId > currentId, "too small");
        currentId = nextId;
    }

    function airdrop(address[] memory list) external onlyOwner {
        uint256 id = currentId;
        for (uint256 i = 0; i < list.length; i++) {
            id++;
            _mint(list[i], id);
        }
        currentId = id;
    }

    function mint(bytes32 message, uint8 v, bytes32 r, bytes32 s) external {
        bytes32 digest = keccak256(abi.encodePacked(msg.sender, message));
        if (minted[digest]) revert RepeatMint();

        address signer = ecrecover(digest, v, r, s);
        if (signer != owner) revert InvalidSigner();

        minted[digest] = true;
        uint256 id = currentId + 1;
        _mint(msg.sender, id);
        currentId = id;
    }
}
